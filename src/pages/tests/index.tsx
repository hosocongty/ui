import { useState, useEffect } from "react";
import { ProfileCard } from "../../components/ProfileCard";
import { SearchInput } from "../../components/SearchInput";
import { data, iProfile } from "../../services/data";
import Link from "next/link";
import { Content } from "../../content/Content";
import { Main } from '../../templates/Main';
import { Meta } from '../../layout/Meta';
import { CityData, iCity } from "../../services/CityData";
import { CityPagination, ICityPaginationProps } from "../../components/CityPagination";
import { GetStaticProps } from 'next';
import { AppConfig } from "../../utils/AppConfig";
import { IPaginationProps } from "../../pagination/Pagination";
import { useRouter } from "next/router";
import { CityNav } from "../../components/CityNav";

const Index = (props: ICityPaginationProps) => {
  // initialize useState for the data
  const [profileData, setProfileData] = useState<iProfile[]>([]);
  const [cityData, setCityData] = useState<iCity[]>([]);

  useEffect(() => {
    // will be updated soon
    setProfileData(data);
    setCityData(CityData);
  }, []);

  // get total users
  const totalUser = profileData.length;
  const router = useRouter()
  return (

    <Main meta={<Meta title="Lorem ipsum" description="Lorem ipsum" />}>


      <Content>

        <div className="grid grid-cols-5 grid-rows-5 gap-12">
          <div className="col-span-4 row-span-5">

            <div className="ml-3 mt-5">
              {profileData.map(
                ({
                  nameCompany,
                  legalRepresentative,
                  taxCode,
                  taxAddress,
                  businessMainLine,
                  phoneNumber,
                  businessActives,
                  tags
                }: iProfile, index) => {
                  return (
                    <div className="mt-5" key={index}>
                        <div>
                          <ProfileCard
                            nameCompany={nameCompany}
                            legalRepresentative={legalRepresentative}
                            taxCode={taxCode}
                            taxAddress={taxAddress}
                            businessMainLine={businessMainLine}
                            phoneNumber={phoneNumber}
                            businessActives={businessActives}
                            tags={tags}
                          />
                        </div>
                    </div>
                  );
                }
              )}
            </div>
          </div>
          <div className="row-span-5 col-start-5">
            <h5> Tinh/TP</h5>
            <CityNav cities={cityData} slug="address" key="" />


          </div>

        </div>




      </Content>
    </Main>
  );
};
export const getStaticProps: GetStaticProps<ICityPaginationProps> = async () => {
  const cities = CityData;
  const pagination: IPaginationProps = {};
  if (cities.length > AppConfig.pagination_size) {
    pagination.next = '/tests/page2';
  }

  return {
    props: {
      cities: cities.slice(0, AppConfig.pagination_size),
      pagination,
    },
  };
};

export default Index;

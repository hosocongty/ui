import React, { useEffect, useState } from 'react';
import { Meta } from '../../layout/Meta';
import { Main } from '../../templates/Main';
import { useRouter } from 'next/router';
import { data, iProfile } from '../../services/data';
import { Table, TableHeader, TableColumn, TableBody, TableRow, TableCell } from "@nextui-org/react";
import * as _ from "lodash";
import Link from 'next/link';
import { ProfileCard } from '../../components/ProfileCard';

export interface ITestDetailProps { }

const TestDetail = (props: ITestDetailProps) => {
  const router = useRouter();
  const [profileData, setProfileData] = useState<iProfile[]>([]);


  useEffect(() => {
    setProfileData(data);
  }, []);

  const taxCode = router.query.testId as string;

  const profile = profileData.find(x => x.taxCode === taxCode);
  const suggestion = _.take(profileData, 10);
  return (
    <Main meta={<Meta title="Lorem ipsum" description="Lorem ipsum" />}>
      {profile && (
        <>
          <Table>
            <TableHeader>
              <TableColumn>{profile.nameCompany}</TableColumn>
            </TableHeader>
            <TableBody>
              <TableRow>
                <TableCell>
                  <em>Tên Công Ty:{profile.nameCompany}</em></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><em>Người Đại Diện Pháp Luật:<Link href="#">{profile.legalRepresentative}</Link></em></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><em>Mã Số Thuế:{profile.taxCode}</em></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><em>Địa Chỉ:{profile.taxAddress}</em></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><em>Số Điện Thoại:{profile.phoneNumber}</em></TableCell>
              </TableRow>
            </TableBody>
          </Table>

          {profile.businessActives && profile.businessActives.length > 0 && (
            <Table>
              <TableHeader>
                <TableColumn>NGÀNH NGHỀ KINH DOANH</TableColumn>
              </TableHeader>
              <TableBody>
                {profile.businessActives.map((item, index) => (
                  <TableRow key={index}>

                    <TableCell>

                      <Link href={`/industry/${item.code}`} key={item.code}>
                        <em>{item.code}  {item.name}</em>
                      </Link>

                    </TableCell>


                  </TableRow>
                ))}
              </TableBody>
            </Table>
          )}
          <br />
          <h1>Tags:</h1>
          <ul>
            {profile.tags.map((item, index) => (
              <React.Fragment key={index}>
                <Link href="#">
                  <a>{item.name}</a>
                </Link>
                {index !== profile.tags.length - 1 && ', '}
              </React.Fragment>
            ))}
          </ul>
          <br />
          <h1> Lien Quan</h1>
          {suggestion.map(
            ({
              nameCompany,
              legalRepresentative,
              taxCode,
              taxAddress,
              businessMainLine,
              phoneNumber,
              businessActives,
              tags,
            }: iProfile, index) => {
              return (
                <div className="mt-5" key={index}>
                  <Link href={`${taxCode}`} key={nameCompany}>
                    <div>
                      <ProfileCard
                        nameCompany={nameCompany}
                        legalRepresentative={legalRepresentative}
                        taxCode={taxCode}
                        taxAddress={taxAddress}
                        businessMainLine={businessMainLine}
                        phoneNumber={phoneNumber}
                        businessActives={businessActives}
                        tags={tags}
                      />
                    </div>
                  </Link>
                </div>
              );
            }
          )}
        </>
      )}
    </Main>
  );
};

export default TestDetail;

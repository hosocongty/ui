'use client'

import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { data, iProfile } from '../../services/data';
import * as _ from 'lodash';
import Link from 'next/link';
import { ProfileCard } from '../../components/ProfileCard';
import { Main } from '../../templates/Main';
import { Meta } from '../../layout/Meta';
import { CityNav } from '../../components/CityNav';
import { CityData, iCity } from '../../services/CityData';

const AddressDetail = () => {
  const router = useRouter();


  const [profileData, setProfileData] = useState<iProfile[]>([]);
  const [cityData, setCityData] = useState<iCity[]>([]);

  useEffect(() => {
    setProfileData(data);
    setCityData(CityData);
  }, []); 

  console.log(JSON.stringify(router.query))

  return (
    <Main meta={<Meta title="Lorem ipsum" description="Lorem ipsum" />}>
      <h1>{JSON.stringify(router.query.Id)}</h1>

      {/* Filter and map profile data */}
      {
        _.filter(profileData, (element) => {
          return element.taxAddress.includes(`${router.query.Id}`);
        }).map(({ nameCompany, legalRepresentative, taxCode, taxAddress, businessMainLine, phoneNumber, businessActives, tags }: iProfile, index) => (
          <div className="mt-5" key={index}>
            <Link href={`/tests/${taxCode}`} key={nameCompany}>
              <div>
                <ProfileCard
                  nameCompany={nameCompany}
                  legalRepresentative={legalRepresentative}
                  taxCode={taxCode}
                  taxAddress={taxAddress}
                  businessMainLine={businessMainLine}
                  phoneNumber={phoneNumber}
                  businessActives={businessActives}
                  tags={tags}
                />
              </div>
            </Link>
          </div>
        ))
      }
    </Main>
  );
};

export default AddressDetail;

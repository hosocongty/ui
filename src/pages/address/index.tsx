
import { useState, useEffect } from "react";
import { Content } from "../../content/Content";
import { Main } from '../../templates/Main';
import { Meta } from '../../layout/Meta';
import { CityData, iCity } from "../../services/CityData";
import Link from "next/link";
import { CityNav } from "../../components/CityNav";

const Index = () => {
  const [cityData, setCityData] = useState<iCity[]>([]);

  useEffect(() => {
    setCityData(CityData);
  }, []);


  return (
    <Main meta={<Meta title="Lorem ipsum" description="Lorem ipsum" />}>
      <Content>
        <div className="grid grid-cols-5 grid-rows-5 gap-12">
          <div className="col-span-4 row-span-5">
            <h5> Tinh/TP</h5>
            <ul>
           
              {cityData.map((elt) => (
                <li className="mb-3 flex justify-between">
                  <Link href="/address/[Id]" as={`/address/${elt.name}`} key={elt.name}>

                    <h2>{elt.name}</h2>
                  </Link>

                </li>
              ))}
            </ul>
          </div>
          <div className="row-span-5 col-start-5">
          <CityNav cities={cityData} slug="address" key=""/>
          </div>
        </div>
      </Content>
    </Main>
  );
};

export default Index;

'use client'

import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { data, iProfile } from '../../services/data';
import * as _ from 'lodash';
import Link from 'next/link';
import { ProfileCard } from '../../components/ProfileCard';
import { Main } from '../../templates/Main';
import { Meta } from '../../layout/Meta';

const Page = ({ params }: any) => {
  console.log(params)
  const router = useRouter();
  const { contactId, name } = router.query
  console.log(contactId)
  console.log(name)

  const [profileData, setProfileData] = useState<iProfile[]>([]);
  const [taxAddress, setTaxAddress] = useState<string | undefined>(undefined);


  console.log(JSON.stringify(router))
  console.log(JSON.stringify(router.query))


  useEffect(() => {
    setProfileData(data);


    if (router.query.addressId) {
      setTaxAddress(router.query.addressId as string);
    }
  }, [router.query.addressId]); // Trigger useEffect when router.query.addressId changes

  if (typeof taxAddress === 'undefined') {
    // Handle the case when taxAddress is not available yet


    // return <div>Loading...</div>;

  }
  const profile = _.filter(data, (element) => {
    return element.taxAddress.includes('Hồ Chí Minh');
  });


  return (
    <Main meta={<Meta title="Lorem ipsum" description="Lorem ipsum" />}>
      <h1>{JSON.stringify(router.query)}</h1>
      {profile.map(
        ({
          nameCompany,
          legalRepresentative,
          taxCode,
          taxAddress,
          businessMainLine,
          phoneNumber,
          businessActives,
          tags
        }: iProfile, index) => {
          return (
            <div className="mt-5" key={index}>
              <Link href={`tests/${taxCode}`} key={nameCompany}>
                <div>
                  <ProfileCard
                    nameCompany={nameCompany}
                    legalRepresentative={legalRepresentative}
                    taxCode={taxCode}
                    taxAddress={taxAddress}
                    businessMainLine={businessMainLine}
                    phoneNumber={phoneNumber}
                    businessActives={businessActives}
                    tags={tags}
                  />
                </div>
              </Link>
            </div>
          );
        }
      )}
    </Main>
  );
};

export default Page;
// pages/users/[userId].js

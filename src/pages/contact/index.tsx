
import { useState, useEffect } from "react";
import { Content } from "../../content/Content";
import { Main } from '../../templates/Main';
import { Meta } from '../../layout/Meta';
import { CityData, iCity } from "../../services/CityData";
import Link from "next/link";

const Index = () => {
  const [cityData, setCityData] = useState<iCity[]>([]);

  useEffect(() => {
    setCityData(CityData);
  }, []);


  return (
    <Main meta={<Meta title="Lorem ipsum" description="Lorem ipsum" />}>
      <Content>
        <div className="grid grid-cols-5 grid-rows-5 gap-12">
          <div className="col-span-4 row-span-5">
            <h5> Tinh/TP</h5>
            <ul>
              <Link
                href={{
                  pathname: '/contact/[contactId]',
                  query: { contactId: '123123' },
                }}
              >
                <h2>qsd</h2>
              </Link>
              {cityData.map((elt) => (
                <li className="mb-3 flex justify-between">
                  <Link
                    href={{
                      pathname: '/contact/[contactId]',
                      query: { contactId: `${elt.name}` },
                    }}
                  >
                    <h2>{elt.name}</h2>
                  </Link>

                </li>
              ))}
            </ul>
          </div>
          <div className="row-span-5 col-start-5">
            {/* <h5> Tinh/TP</h5>
            <ul>
              {cityData.map((elt) => (
                <Link href={{
                  pathname: `/address/${elt.name}`,
                  query: { addressId: elt.name }
                }} key={elt.name}>
                  <li className="mb-3 flex justify-between">
                    <h2>{elt.name}</h2>
                  </li>
                </Link>
              ))}
            </ul> */}
          </div>
        </div>
      </Content>
    </Main>
  );
};

export default Index;





import Link from 'next/link';
import { IPaginationProps, Pagination } from '../pagination/Pagination';
import { iCity, CityData } from '../services/CityData';


export type ICityNavProps = {
  key: string;
  cities: iCity[];
  slug: string
};

const CityNav = (props: ICityNavProps) => (
  <>
    <ul>
      {props.cities && props.cities.map((elt) => (
        <Link href={`/${props.slug}/${elt.name}`}>
          <li key={elt.name} className="mb-3 flex justify-between">

            <h2>{elt.name}</h2>


          </li>
        </Link>
      ))}
    </ul>

  </>
);

export { CityNav };

import Image from 'next/image'


//Import the profile interface from data.js


import { iProfile } from "../services/data";
import Link from 'next/link';



export const ProfileCard = (props: iProfile) => {


  const { nameCompany, legalRepresentative, taxCode, taxAddress, businessMainLine, phoneNumber, businessActives, tags } = props;


  return (

    <div className="profile__card rounded-[15px] border border-solid">



      <div className=" bg-white	 p-3">
        <ul>

          <li>
            <Link href={`tests/${taxCode}`} key={nameCompany}>
              <a>
                {nameCompany}
              </a>
            </Link>

          </li>

          <li><em>Địa Chỉ:{taxAddress}</em></li>
          <li><em>Mã Số Thuế:{taxCode}</em></li>

        </ul>






      </div>

    </div>

  )

}
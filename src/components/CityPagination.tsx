



import Link from 'next/link';
import { IPaginationProps, Pagination } from '../pagination/Pagination';
import { iCity, CityData } from '../services/CityData';


export type ICityPaginationProps = {
  cities: iCity[];
  pagination: IPaginationProps;
};

const CityPagination = (props: ICityPaginationProps) => (
  <>
    <ul>
      {props.cities && props.cities.map((elt) => (
        <Link href={`/address/${elt.name}`}>
        <li key={elt.name} className="mb-3 flex justify-between">
            
              <h2>{elt.name}</h2>

         
        </li>
        </Link>
      ))}
    </ul>

    <Pagination
      previous={props.pagination.previous}
      next={props.pagination.next}
    />
  </>
);

export { CityPagination };

export function convertTo2D<T>(arr: T[], size: number) {
  const res: T[][] = [];

  arr.forEach((element, index) => {
    if (index % size === 0) {
      res.push([element]);
    } else {
      res[res.length - 1].push(element);
    }
  });

  return res;
}

export function convertUrlToLinkHref(url: string) {
  console.log(url)
  if (url === '/') {
    return '/';
  }
  
  if (url.includes('/tests/')) {
    return '/tests/[page]';
  }
  return '/[page]';
}
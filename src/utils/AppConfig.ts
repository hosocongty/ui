export const AppConfig = {
  site_name: 'Home',
  title: 'Next.js Boilerplate',
  description:
    'Website help client search for information company',
  url: 'https://hosocongty.info',
  locale: 'en',
  author: 'Anonymous',
  pagination_size: 5,
};

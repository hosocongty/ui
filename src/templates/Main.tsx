import React, { ReactNode } from 'react';

import Link from 'next/link';
import Image from 'next/image'

import { Navbar } from '../navigation/Navbar';
import { AppConfig } from '../utils/AppConfig';
import { SearchInput } from '../components/SearchInput';
import homeImg from '../../public/assets/images/home.png';

type IMainProps = {
  meta: ReactNode;
  children: ReactNode;
};

const Main = (props: IMainProps) => (
  <div className="antialiased w-full text-gray-700 px-3 md:px-0">

    {props.meta}

    <div className="max-w-screen-2xl mx-auto bg-slate-100">
      <div className="border-b border-gray-300">
        <div className="pt-4 pb-2">
        </div>
        <div>
          <Navbar>
            <li className="ml-3 mr-6">
              <Link href="/tests">
                <a className="text-3xl font-black text-center text-justify">hosocongty.info</a>
              </Link>
            </li>

            <li className="mr-6">
              <Link href="/tests">
                <a>Trang Chủ</a>
              </Link>
            </li>
            <li className="mr-6">
              <Link href="/">
                <a>Khu Vực</a>
              </Link>
            </li>
            <li className="mr-6">
              <Link href="/">
                <a>Ngành Nghề</a>
              </Link>
            </li>
          </Navbar>
          <div className="grid justify-items-stretch ...">
            <div className='justify-self-start ...'>

            </div>
            <div >
              <SearchInput defaultValue={""} />

            </div>
            <div>
            </div>
          </div>
        </div>
      </div>

      <div className="text-xl py-5">{props.children}</div>

      <div className="border-t border-gray-300 text-center py-8 text-sm">
        © Copyright {new Date().getFullYear()} {AppConfig.title}. Powered with{' '}
        <span role="img" aria-label="Love">
          ♥
        </span>{' '}
        by <a href="https://creativedesignsguru.com">CreativeDesignsGuru</a>
        {/*
         * PLEASE READ THIS SECTION
         * We'll really appreciate if you could have a link to our website
         * The link doesn't need to appear on every pages, one link on one page is enough.
         * Thank you for your support it'll mean a lot for us.
         */}
      </div>
    </div>
  </div>
);

export { Main };
